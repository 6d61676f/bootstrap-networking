#ifndef STP_H
#define STP_H

#include <net/if.h>
#include <stdint.h>
#include <sys/ioctl.h>
#define STP_PROTO 118
#define ETHER_SIZE 14
#define LLC_SIZE 3
#define STP_HDR_SIZE 35
#define RSTP_HDR_SIZE 36
#define STP_SIZE 52
#define RSTP_SIZE 53

enum STP_TYPE
{
    STP,
    RSTP
};

struct __attribute__((__packed__)) etherFrame_t
{
    uint8_t macDst[6];
    uint8_t macSrc[6];
    uint16_t eth2Type;
};
struct __attribute__((__packed__)) LogicalLinkControl_t
{
    uint8_t DSAP;
    uint8_t SSAP;
    uint8_t controlField;
};
struct __attribute__((__packed__)) STPhdr_t
{
    uint16_t protocolIdentifier;       // 0x0000   2
    uint8_t protocolVersionIdentifier; // 0x01 / 0x02 1
    uint8_t BPDUtype;                  // 1
    uint8_t BPDUflags;                 // 1
    union
    {
        uint8_t rootIdentifier8[8];
        uint32_t rootIdentifier32[2];
        struct
        {
            uint16_t rootBridgePriority; // 1
            uint8_t rootBridgeSysId[6];  // 6
        } rootIdStruct;
    }; // 8

    uint32_t rootPathCost; // 4
    union
    {
        uint8_t bridgeIdentifier8[8];
        uint32_t bridgeIdentifier32[2];
        struct
        {
            uint16_t bridgePriority;
            uint8_t bridgeSysId[6];
        } bridgeIdStruct;
    }; // 8

    uint16_t portIdentifier; // 2
    uint16_t messageAge;     // 2
    uint16_t maxAge;         // 2
    uint16_t helloTime;      // 2
    uint16_t fwdDelay;       // 2
    uint8_t versionLength;   //? 1 Only for RSTP
};                           // Should be 35/36

struct __attribute__((__packed__)) STPpacket_t
{
    struct etherFrame_t frame;
    struct LogicalLinkControl_t llc;
    struct STPhdr_t STPhead;
};

int createStpPack(struct STPpacket_t** stp, enum STP_TYPE type);
int deleteStpPack(struct STPpacket_t** stp);
int initStpPack(struct STPpacket_t* stp);
int initRstpPack(struct STPpacket_t* stp);
int getMac(const char interfaceName[], struct ifreq* interface);
int populateSrcMac(struct STPpacket_t* stp, uint8_t* srcMac);
int generatePortIdentifier(struct STPpacket_t* stp, const char interfaceName[]);

#endif
