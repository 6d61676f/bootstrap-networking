#include "stp.h"
#include <arpa/inet.h>
#include <assert.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
int
main(int argc, char* argv[])
{

    if (getuid() != 0) {
        fprintf(stderr, "Let's get root first\n");
        return 1;
    }
    if (argc < 2) {
        fprintf(stderr, "%s interface [count]\n", argv[0]);
        return (1);
    }

    int count = 10;
    if (argc > 2) {
        count = atoi(argv[2]);
    }

    int sock = socket(AF_PACKET, SOCK_RAW, htons(STP_PROTO));
    assert(sock > 0);

    struct ifreq ifReq;
    // Must always be a null pointer
    struct STPpacket_t* stpPack = 0;
    createStpPack(&stpPack, STP);
    assert(stpPack != 0);
    assert(getMac("eno1", &ifReq) == 0);
    assert(
      populateSrcMac(stpPack, (uint8_t*)ifReq.ifr_ifru.ifru_addr.sa_data) == 0);
    stpPack->STPhead.portIdentifier = generatePortIdentifier(stpPack, argv[1]);
    assert(stpPack->STPhead.portIdentifier < 0xFFFF);

    struct sockaddr_ll sockLL;
    memset(&sockLL, 0x0, sizeof sockLL);
    sockLL.sll_family = AF_PACKET;
    sockLL.sll_protocol = STP_PROTO;
    sockLL.sll_ifindex = if_nametoindex(argv[1]);
    sockLL.sll_halen = 6;
    // Merge si fara !?
    //    sockLL.sll_addr[0] = 0x01;
    //    sockLL.sll_addr[1] = 0x80;
    //    sockLL.sll_addr[2] = 0xc2;
    //    sockLL.sll_addr[3] = 0x00;
    //    sockLL.sll_addr[4] = 0x00;
    //    sockLL.sll_addr[5] = 0x00;

    size_t sent = 0;
    while (count > 0) {
        sent = sendto(sock, stpPack, STP_SIZE, 0,
                      (const struct sockaddr*)&sockLL, sizeof sockLL);
        assert(sent == STP_SIZE);
        count--;
    }
    deleteStpPack(&stpPack);
    assert(stpPack == 0);
    close(sock);

    return 0;
}
