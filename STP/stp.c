#include "stp.h"
#include <arpa/inet.h>
#include <malloc.h>
#include <net/if.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

int
createStpPack(struct STPpacket_t** stp, enum STP_TYPE type)
{
    int state = 1;
    if (stp == 0) {
        state = 1;
    } else {
        if (*stp) {
            deleteStpPack(stp);
        }
        *stp = calloc(1, sizeof **stp);
        if (type == STP) {
            state = initStpPack(*stp);
        } else {
            state = initRstpPack(*stp);
        }
    }
    return (state);
}
int
deleteStpPack(struct STPpacket_t** stp)
{
    if (stp && *stp) {
        free(*stp);
        *stp = 0;
        return (0);
    } else {
        return (1);
    }
}
int
initStpPack(struct STPpacket_t* stp)
{
    if (!stp) {
        return (1);
    } else {
        stp->frame.eth2Type = htons(LLC_SIZE + STP_HDR_SIZE);
        // Big Endian
        //*((uint64_t*)stp->frame.macDst) = 0x0180c2000000;
        // Little Endian
        *((uint64_t*)stp->frame.macDst) = 0x000000c28001;

        stp->llc.DSAP = 0x42;
        stp->llc.SSAP = 0x42;
        stp->llc.controlField = 0x03;

        stp->STPhead.protocolIdentifier = 0x0000;
        stp->STPhead.protocolVersionIdentifier = 0x00;
        stp->STPhead.BPDUtype = 0x00;
        stp->STPhead.BPDUflags = (0x01 << 7) | 0x01;
        stp->STPhead.rootIdStruct.rootBridgePriority = 0x10;

        stp->STPhead.rootPathCost = 0x0;

        stp->STPhead.messageAge = 0x00;
        stp->STPhead.maxAge = 20;
        stp->STPhead.helloTime = 0x02;
        stp->STPhead.fwdDelay = 0x01;

        return (0);
    }
}
int
initRstpPack(struct STPpacket_t* stp)
{
    if (!stp) {
        return (1);
    } else {
        stp->frame.eth2Type = htons(LLC_SIZE + RSTP_HDR_SIZE);
        // Big Endian
        //*((uint64_t*)stp->frame.macDst) = 0x0180c2000000;
        // Little Endian
        *((uint64_t*)stp->frame.macDst) = 0x000000c28001;

        stp->llc.DSAP = 0x42;
        stp->llc.SSAP = 0x42;
        stp->llc.controlField = 0x03;

        stp->STPhead.protocolIdentifier = 0x0000;
        stp->STPhead.protocolVersionIdentifier = 0x00;
        stp->STPhead.BPDUtype = 0x00;
        stp->STPhead.BPDUflags = (0x01 << 7) | 0x01;
        stp->STPhead.rootIdStruct.rootBridgePriority = 0x10;

        stp->STPhead.rootPathCost = 0x0;

        stp->STPhead.messageAge = 0x00;
        stp->STPhead.maxAge = 20;
        stp->STPhead.helloTime = 0x02;
        stp->STPhead.fwdDelay = 0x01;
        stp->STPhead.versionLength = 0x0; // just for RSTP

        return (0);
    }
}
int
getMac(const char interfaceName[], struct ifreq* interface)
{
    if (interfaceName == 0 || interface == 0) {
        return (-1);
    }
    int s = socket(PF_INET, SOCK_DGRAM, 0);
    if (s <= 0) {
        return (-1);
    }
    int err = 0;
    memset(interface, 0x00, sizeof *interface);
    strncpy(interface->ifr_ifrn.ifrn_name, interfaceName, IFNAMSIZ);
    err = ioctl(s, SIOCGIFHWADDR, interface);
    close(s);
    if (err != -1) {
        err = 0;
    }
    return (err);
}
int
populateSrcMac(struct STPpacket_t* stp, uint8_t* srcMac)
{
    if (!srcMac || !stp) {
        return (-1);
    } else {
        memmove(stp->frame.macSrc, srcMac, 6);
        memmove(stp->STPhead.bridgeIdStruct.bridgeSysId, srcMac, 6);
        memmove(stp->STPhead.rootIdStruct.rootBridgeSysId, srcMac, 6);
        return (0);
    }
}
int
generatePortIdentifier(struct STPpacket_t* stp, const char interfaceName[])
{
    int intNo = -1;
    if (!stp || !interfaceName) {
        return (-1);
    } else {
        intNo = if_nametoindex(interfaceName);
        if (!intNo) {
            return (-1);
        }
        // Should handle endianness
        intNo =
          ((intNo & 0xFF) << 8) | stp->STPhead.rootIdStruct.rootBridgePriority;
    }
    return (intNo);
}
