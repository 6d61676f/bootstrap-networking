#include "subnetting.h"
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int
main(int argc, char* argv[])
{
  if (argc != 2) {
    fprintf(stderr, "%s %s %s\n", "Apel de forma", argv[0], "192.168.1.1/24");
    return (EXIT_FAILURE);
  }

  uint8_t adresa[4];
  char* point;
  char* addr;
  uint8_t bits;

  addr = strtok_r(argv[1], "./", &point);

  if (addr != 0) {
    adresa[0] = strtoul(addr, 0, 10);
  } else {
    fprintf(stderr, "%s\n", "Ip invalid");
    return (EXIT_FAILURE);
  }

  for (int i = 1; i < 4 && addr != 0; i++) {
    addr = strtok_r(0, "./", &point);
    if (addr != 0) {
      adresa[i] = strtoul(addr, 0, 10);
    } else {
      fprintf(stderr, "%s\n", "Ip invalid");
      return (EXIT_FAILURE);
    }
  }

  addr = strtok_r(0, "./", &point);
  if (addr != 0) {
    bits = strtoul(addr, 0, 10);
    if (bits > 30) {
      fprintf(stderr, "%s\n", "Maskbits invalid");
      return (EXIT_FAILURE);
    }
  } else {
    fprintf(stderr, "%s\n", "Maskbits invalid");
    return (EXIT_FAILURE);
  }

  uint32_t adresa32 = init32AddrArray(adresa);
  uint32_t first = getFirstAddr(adresa32, bits);
  uint32_t last = getLastAddr(adresa32, bits);
  uint32_t broad = getBroadAddr(adresa32, bits);
  uint32_t subnet = getSubnetMask(bits);

  printf("%s\n", "Adresa IP este:");
  print32addr(adresa32);

  printf("\n%s\n", "Adresa subnet este:");
  print32addr(subnet);

  printf("\n%s\n", "Primul IP adresabil este:");
  print32addr(first);

  printf("\n%s\n", "Ultimul IP adresabil este:");
  print32addr(last);

  printf("\n%s\n", "Adresa broadcast este:");
  print32addr(broad);

  printf("\n%s %u\n", "Indicele primului bit setat este:",
         getFirstBit(adresa32));
  printf("%s %u %s\n", "Adresa are", countBits(adresa32), " biti setati");

  return (EXIT_SUCCESS);
}
