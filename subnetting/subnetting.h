#ifndef SUB_H
#define SUB_B

#include <stdint.h>

void print32addr(uint32_t);
uint32_t getFirstAddr(uint32_t, uint8_t);
uint32_t getLastAddr(uint32_t, uint8_t);
uint32_t getBroadAddr(uint32_t, uint8_t);
uint32_t getSubnetMask(uint8_t);
uint32_t init32Addr(uint8_t, uint8_t, uint8_t, uint8_t);
uint32_t init32AddrArray(uint8_t[]);
uint8_t countBits(uint32_t);
uint8_t getFirstBit(uint32_t);

#endif // SUB_H
