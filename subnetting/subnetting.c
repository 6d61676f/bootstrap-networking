#include "subnetting.h"
#include <stdio.h>
#include <stdlib.h>

const uint32_t FF = 0xFFFFFFFF;
const uint32_t AMASK = 0xFF000000;
const uint32_t BMASK = 0xFFFF0000;
const uint32_t CMASK = 0xFFFFFF00;

uint32_t
getSubnetMask(uint8_t maskBits)
{
  uint32_t first = (FF << (32 - maskBits));
  return first;
}

void
print32addr(uint32_t adresa)
{
  uint8_t* p = (uint8_t*)&adresa;
  for (int i = 3; i >= 0; i--) {
    printf("%d%c", p[i], (i > 0) ? ('.') : ('\n'));
  }
}
uint32_t
getFirstAddr(uint32_t adresa, uint8_t maskBits)
{
  uint32_t first = getSubnetMask(maskBits);
  adresa &= (first);
  return (adresa + 1);
}
uint32_t
getLastAddr(uint32_t adresa, uint8_t maskBits)
{
  return (getBroadAddr(adresa, maskBits) - 1);
}
uint32_t
getBroadAddr(uint32_t adresa, uint8_t maskBits)
{
  uint32_t first = getSubnetMask(maskBits);
  adresa |= ~(first);
  return (adresa);
}
uint32_t
init32Addr(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
  uint8_t adresa[4];
  uint32_t* adresa32 = (uint32_t*)&adresa;
  adresa[0] = d;
  adresa[1] = c;
  adresa[2] = b;
  adresa[3] = a;
  return (*adresa32);
}
uint32_t
init32AddrArray(uint8_t* adresa)
{
  uint8_t reverse[4];
  uint32_t* adresa32 = (uint32_t*)&reverse;
  for (unsigned i = 0; i < 4; i++) {
    reverse[i] = adresa[4 - (i + 1)];
  }
  return (*adresa32);
}
uint8_t
countBits(uint32_t adresa)
{
  uint8_t c = 0;
  for (unsigned i = 0; i < 32; i++) {
    c += (adresa & 1);
    adresa >>= 1;
  }
  return (c);
}
uint8_t
getFirstBit(uint32_t adresa)
{
  uint8_t poz = 0;
  for (unsigned i = 0; i < 32; i++) {
    if (adresa & 1) {
      poz = i;
      break;
    } else {
      adresa >>= 1;
    }
  }
  return (poz);
}
