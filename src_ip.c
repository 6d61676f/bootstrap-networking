#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define MSG "Salut!"
#define MSG_LEN strlen(MSG) + 1
#define IP_SRC "1.1.1.1"
#define IP_DST "10.0.0.50"
#define PORT_DST 5050

#define TCP 0

//NOTE
//Demo application to show the option of changing the source ip in a TCP/UDP packet
//THE SRC IP MUST EXIST!
int
main(void)
{
  int ret = EXIT_FAILURE;

  struct msghdr msg;
  memset(&msg, 0x00, sizeof msg);

  struct iovec* iov = NULL;
  struct cmsghdr* cmsg = NULL;
  struct in_pktinfo* pkt_info = NULL;
  struct sockaddr_in* addr = NULL;
#if TCP
  const int s = socket(AF_INET, SOCK_STREAM, 0);
#else
  const int s = socket(AF_INET, SOCK_DGRAM, 0);
#endif

  if (s < 0) {
    printf("Socket not ok\n");
    goto fail;
  }

  msg.msg_name = calloc(1, sizeof(struct sockaddr_in));
  addr = msg.msg_name;
  memset(addr, 0x00, sizeof(struct sockaddr_in));

  addr->sin_family = AF_INET;
  addr->sin_port = htons(PORT_DST);
  if (inet_pton(AF_INET, IP_DST, &addr->sin_addr) != 1) {
    printf("pton failed\n");
    goto fail;
  }

#if TCP
  if (connect(s, (const struct sockaddr*)addr, sizeof *addr) != 0) {
    perror("connect");
    goto fail;
  }
#endif

  msg.msg_namelen = sizeof(struct sockaddr_in);
  msg.msg_iov = calloc(1, sizeof(struct iovec));
  msg.msg_iovlen = 1;
  iov = msg.msg_iov;
  iov->iov_base = &MSG;
  iov->iov_len = MSG_LEN;

  msg.msg_controllen = CMSG_LEN(sizeof(struct in_pktinfo));
  msg.msg_control = calloc(1, CMSG_LEN(sizeof(struct in_pktinfo)));
  cmsg = msg.msg_control;
  cmsg->cmsg_len = msg.msg_controllen;
  cmsg->cmsg_level = SOL_IP;
  cmsg->cmsg_type = IP_PKTINFO;

  pkt_info = (struct in_pktinfo*)CMSG_DATA(cmsg);
  pkt_info->ipi_ifindex = 0;
  if (inet_pton(AF_INET, IP_SRC, &pkt_info->ipi_spec_dst) != 1) {
    printf("Could not set local addr\n");
    goto fail;
  }

#if 0 // This can ne skipped, it seems
  if (inet_pton(AF_INET, IP_DST, &pkt_info->ipi_addr) != 1) {
    printf("Could not set dst addr\n");
    goto fail;
  }
#endif

  if (sendmsg(s, &msg, 0) < 0) {
    printf("Could not send...errno(%d)\n", errno);
    perror("sendmsg");
    goto fail;
  }

  ret = EXIT_SUCCESS;
fail:
  if (cmsg) {
    free(cmsg);
  }
  if (iov) {
    free(iov);
  }
  if (addr) {
    free(addr);
  }
  if (s >= 0) {
    close(s);
  }

  return ret;
}
