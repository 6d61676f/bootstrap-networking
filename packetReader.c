#include "hacking.h"
#include <getopt.h>
#include <linux/if_packet.h>
#include <malloc.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define PACK_SIZE 0xFFFF
#define DUMP_SIZE 0xFFFF

int
main(int argc, char* argv[])
{

  char help[0x1000];
  sprintf(help, "Exemplu de apelare default:\n%s -e 0x0806 -c -1 "
                "-o cale_output"
                "\n%s --ethertype 0x0800 --count 2 --output cale_fisier",
          argv[0], argv[0]);
  int ch = 0;
  int etherII = ETH_P_ALL;
  int count = -1;
  int options_index = 0;
  FILE* output = 0;
  static struct option long_options[] = {
    { "ethertype", required_argument, 0, 'e' },
    { "count", required_argument, 0, 'c' },
    { "output", required_argument, 0, 'o' },
    { "help", no_argument, 0, 'h' },
    { 0, 0, 0, 0 }
  };
  while ((ch = getopt_long(argc, argv, "e:c:o:h", long_options,
                           &options_index)) != -1) {
    switch (ch) {
      case 'o':
        output = fopen(optarg, "ab");
        chmod(optarg, 0666);
        break;
      case 'c':
        if ((count = strtol(optarg, 0, 10)) == 0) {
          fprintf(stderr, "%s este invalid. Folositi -1 pentru infinit\n",
                  optarg);
          exit(EXIT_FAILURE);
        }
        break;
      case 'e':
        if ((etherII = strtol(optarg, 0, 16)) <= 0) {
          fprintf(stderr, "%s este invalid.\n", optarg);
          exit(EXIT_FAILURE);
        }
        break;

      case '?':
      case 'h':
      default:
        puts(help);
        exit(EXIT_FAILURE);
    }
  }
  if (getuid() != 0) {
    fprintf(stderr, "%s\n", "Avem nevoie de root pentru pachete RAW");
    exit(EXIT_FAILURE);
  }
  int sockFD = socket(PF_PACKET, SOCK_RAW, htons(etherII));
  if (sockFD < 0) {
    fprintf(stderr, "%s\n", "Eroare la deschidere socket");
    exit(EXIT_FAILURE);
  }
  uint8_t citit[PACK_SIZE] = { 0 };
  int i = 0;
  while (i != count) {

    if (recv(sockFD, citit, PACK_SIZE, 0) < 0) {
      fprintf(stderr, "%s\n", "Eroare recv");
    } else {
      dump(citit, DUMP_SIZE, output);
    }
    i++;
  }
  if (output != 0) {
    fclose(output);
  }
  return (EXIT_SUCCESS);
}
