#ifndef HACKING_H
#define HACKING_H

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>

void dump(const uint8_t* buffer, const size_t len, FILE* output);
bool isLittleEndian(void);

bool
isLittleEndian(void)
{
  uint16_t one = 0x0001;
  uint8_t* probe = (uint8_t*)&one;
  return (*probe == one);
}

void
dump(const uint8_t* buffer, const size_t len, FILE* output)
{
  uint8_t byte = 0x00;
  unsigned i, j;
  for (i = 0; i < len; i++) {
    byte = buffer[i];
    printf("%02x ", byte);
    if (output != 0) {
      fwrite(&byte, sizeof byte, 1, output);
    }
    if ((i % 16 == 15) || (i == len - 1)) {
      for (j = 0; j < 15 - (i % 16); j++, printf("%3c", ' '))
        ;
      printf("| ");
      for (j = (i - (i % 16)); j <= i; j++) {
        byte = buffer[j];
        if (isprint(byte)) {
          printf("%c ", byte);
        } else {
          printf(". ");
        }
      }
      puts("");
    }
  }
  if (output != 0) {
    fflush(output);
  }
}

#endif
