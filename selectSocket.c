#include <arpa/inet.h>
#include <malloc.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#define MESAJ "Hello bro\n"
#define BUFFSIZE 0x1000
#define maxConexiune 10

int
main(void)
{
  char buffer[BUFFSIZE];
  int fds[maxConexiune];
  int masterFD = -1;
  int nfds = 0;
  struct sockaddr_in sockAddr;
  int rezultat = 0;
  fd_set fdSet;
  struct timeval timeOut;
  unsigned i;
  memset(&sockAddr, 0x00, sizeof sockAddr);
  for (i = 0; i < maxConexiune; i++) {
    fds[i] = -1;
  }
  if ((masterFD = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    printf("%s\n", "Eroare la socket");
    return (EXIT_FAILURE);
  }
  sockAddr.sin_family = AF_INET;
  sockAddr.sin_port = htons(12345);
  sockAddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
  if (setsockopt(
        masterFD, SOL_SOCKET, SO_REUSEADDR, (void*)(int[]){ 1 }, sizeof(int)) !=
      0) {
    printf("%s\n", "Eroare la REUSEADDR");
    return (EXIT_FAILURE);
  }
  if (setsockopt(
        masterFD, SOL_SOCKET, SO_REUSEPORT, (void*)(int[]){ 1 }, sizeof(int)) !=
      0) {
    printf("%s\n", "Eroare la REUSEPORT");
    return (EXIT_FAILURE);
  }
  if (bind(masterFD, (const struct sockaddr*)&sockAddr, sizeof sockAddr) < 0) {
    printf("%s\n", "Eroare la bind");
    return (EXIT_FAILURE);
  }
  if (listen(masterFD, maxConexiune) < 0) {
    printf("%s\n", "Eroare la listen");
    return (EXIT_FAILURE);
  }
  while (1) {
    FD_ZERO(&fdSet);
    FD_SET(masterFD, &fdSet);
    nfds = masterFD;
    timeOut.tv_sec = 1;
    timeOut.tv_usec = 0;
    for (i = 0; i < maxConexiune; i++) {
      if (fds[i] > -1) {
        FD_SET(fds[i], &fdSet);
        if (fds[i] > nfds) {
          nfds = fds[i];
        }
      }
    }
    rezultat = select(nfds + 1, &fdSet, 0, 0, &timeOut);
    if (rezultat < 0) {
      printf("%s\n", "Eroare la select");
      exit(1);
    } else if (rezultat > 0) {
      if (FD_ISSET(masterFD, &fdSet)) {
        int nou = accept(masterFD, 0, 0);
        printf("%s\n", "Conexiune noua");
        send(nou, MESAJ, strlen(MESAJ) + 1, 0);
        FD_SET(nou, &fdSet);
        for (i = 0; i < maxConexiune; i++) {
          if (fds[i] == -1) {
            fds[i] = nou;
            break;
          }
        }
      } else {
        for (i = 0; i < maxConexiune; i++) {
          if (fds[i] > -1 && FD_ISSET(fds[i], &fdSet)) {
            int citit;
            if ((citit = read(fds[i], buffer, BUFFSIZE)) <= 0) {
              printf("%s %d\n", "S-a deconectat", fds[i]);
              close(fds[i]);
              fds[i] = -1;
            } else {
              buffer[citit] = 0;
              printf("%d: %s\n", fds[i], buffer);
            }
          }
        }
      }
    } else {
      printf("Timeout...\n");
    }
  }
}
