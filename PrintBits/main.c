#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bitset.h"

int
main(int argc, char** argv)
{
  if (argc != 2) {
    printf("Apelare: %s numar\n", argv[0]);
    return (EXIT_FAILURE);
  } else {
    bitset64_t bits = getBits(strtol(argv[1], 0, 10));
    printBitset(bits);
    return (EXIT_SUCCESS);
  }
}
