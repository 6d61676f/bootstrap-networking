#ifndef BITSET_H
#define BITSET_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct bitset64
{
  bool bits[64];
};

typedef struct bitset64 bitset64_t;

bitset64_t getBits(uint64_t);
void printBitset(bitset64_t);
uint64_t getNumber(bitset64_t);

#endif
