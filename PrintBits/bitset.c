#include "bitset.h"

bitset64_t
getBits(uint64_t number)
{
  bitset64_t bitNo = {.bits = { 0 } };
  for (uint8_t i = 0; i < 64; i++) {
    bitNo.bits[i] |= (number & 1);
    number >>= 1;
  }
  return (bitNo);
}
uint64_t
getNumber(bitset64_t bitNo)
{
  uint64_t number = 0;
  for (int i = 63; i >= 0; i--) {
    number |= (bitNo.bits[i] << i);
  }
  return (number);
}
void
printBitset(bitset64_t bitNumber)
{
  int start = 0;
  for (int i = 63; i >= 0; i--) {
    if (bitNumber.bits[i] & 1) {
      start = i;
      break;
    }
  }
  for (int i = start; i >= 0; i--) {
    printf("%c%-2d", '|', i);
  }
  printf("\n");
  for (int i = start; i >= 0; i--) {
    printf("%c%-2u", '|', bitNumber.bits[i]);
  }
  printf("\n");
}
