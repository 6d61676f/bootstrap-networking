#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>

#define REQUEST "HEAD / HTTP/1.0\n\n"

int
main(int argc, char* argv[])
{

  struct addrinfo hintServer = { 0 };
  struct addrinfo* server = 0;
  struct addrinfo* next = 0;
  struct timeval timeout = { 0 };
  uint8_t byte;
  int i = 0;

  hintServer.ai_flags |= AI_CANONNAME;
  hintServer.ai_family = AF_INET;
  hintServer.ai_protocol = IPPROTO_TCP;
  hintServer.ai_socktype = SOCK_STREAM;

  timeout.tv_sec = 5;

  if (argc != 2) {
    fprintf(stderr, "Apelare de tipul %s adresaWeb\n", argv[0]);
    return EXIT_FAILURE;
  }
  int sockFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (sockFD < 0) {
    fprintf(stderr, "%s\n", "Eroare la socket");
    return EXIT_FAILURE;
  }

  if (setsockopt(sockFD, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout,
                 sizeof timeout) < 0) {
    fprintf(stderr, "%s\n", "Eroare la timeout primire");
    return EXIT_FAILURE;
  }

  if (setsockopt(sockFD, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout,
                 sizeof timeout) < 0) {
    fprintf(stderr, "%s\n", "Eroare la timeout trimitere");
    return EXIT_FAILURE;
  }

  if (getaddrinfo(argv[1], "80", (const struct addrinfo*)&hintServer,
                  &server) != 0) {
    fprintf(stderr, "%s\n", "Eroare la getaddrinfo");
    return EXIT_FAILURE;
  }
  if (connect(sockFD, server->ai_addr, sizeof(struct sockaddr)) < 0) {
    fprintf(stderr, "%s\n",
            (errno == EINPROGRESS) ? "Timeout" : "Eroare la connect");
    freeaddrinfo(server);
    return EXIT_FAILURE;
  }
  if (sendto(sockFD, REQUEST, strlen(REQUEST), 0, server->ai_addr,
             sizeof(struct sockaddr)) < 0) {
    fprintf(stderr, "%s\n", "Nu am putut trimite");
    freeaddrinfo(server);
    return EXIT_FAILURE;
  }

  printf("Pentru \'%s\' avem adresele:\n", argv[1]);
  for (next = server; next != 0; next = next->ai_next) {
    const struct sockaddr_in* val = (const struct sockaddr_in*)next->ai_addr;
    printf("%d. %s == %s\n", ++i, next->ai_canonname, inet_ntoa(val->sin_addr));
  }
  puts("");
  while (recv(sockFD, &byte, sizeof byte, 0) == 1) {
    printf("%c", byte);
  }
  freeaddrinfo(server);
  return EXIT_SUCCESS;
}
