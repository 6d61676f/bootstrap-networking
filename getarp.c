#include <arpa/inet.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
int main(int argc, char* argv[]) {

  if (argc != 3) {
    fprintf(stderr, "%s: %s %s %s\n", "Apel de tipul", argv[0],
            "nume interfata", "adresa IPv4");
    return (EXIT_FAILURE);
  }

  int sDesc;
  struct arpreq arpReq;
  struct sockaddr_in* sin;
  uint8_t* adresaHW = (uint8_t*)arpReq.arp_ha.sa_data;
  int i;

  memset(&arpReq, 0x00, sizeof arpReq);

  sin = (struct sockaddr_in*)&arpReq.arp_pa;
  sin->sin_family = AF_INET;

  strncpy(arpReq.arp_dev, argv[1], sizeof(arpReq.arp_dev));
  arpReq.arp_dev[sizeof(arpReq.arp_dev) - 1] = '\0';

  if (inet_pton(AF_INET, argv[2], &(sin->sin_addr)) != 1) {
    fprintf(stderr, "%s: %s\n", "IPv4 invalid", argv[2]);
    return (EXIT_FAILURE);
  }

  if ((sDesc = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
    fprintf(stderr, "%s\n", "Nu am putut crea socket-ul");
    return (EXIT_FAILURE);
  }

  if (ioctl(sDesc, SIOCGARP, &arpReq) < 0) {
    fprintf(stderr, "%s\n", "Eroare la ioctl. IP-ul nu se afla in tabela ARP "
                            "sau interfata de retea nu este corecta");
    return (EXIT_FAILURE);
  }

  printf("%s -> ", inet_ntoa(sin->sin_addr));
  for (i = 0; i < 6; i++) {
    printf("%02X%c", adresaHW[i], (i != 5) ? ':' : '\n');
  }

  return (EXIT_SUCCESS);
}
