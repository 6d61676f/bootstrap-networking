#include <arpa/inet.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int
pollfdCleanup(struct pollfd* pfd, nfds_t len)
{
  if (!pfd) {
    return EXIT_FAILURE;
  }
  for (nfds_t i = 0; i < len; i++) {
    if (pfd[i].fd != -1) {
      close(pfd[i].fd);
      pfd[i].fd = -1;
    }
  }
  return EXIT_SUCCESS;
}

enum
{
  MASTER_SOCKET = 512,
  MAX_SOCK = 512 + 1,
  MAX_BUFFER = 0x1000,
};

int
main(void)
{
  const nfds_t maxSo = MAX_SOCK;
  struct pollfd fds[MAX_SOCK] = { 0x00 };
  struct sockaddr_in socka = { 0x00 };
  int masterSo = -1;
  int newSo = -1;
  int OK = 1;
  ssize_t citit = -1;
  char buffer[MAX_BUFFER] = { 0x00 };

  socka.sin_family = AF_INET;
  socka.sin_port = htons(12345);
  socka.sin_addr.s_addr = htonl(INADDR_ANY);

  if ((masterSo = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    fprintf(stderr, "Could not create socket.\n");
    goto fail;
  }
  if (setsockopt(
        masterSo, SOL_SOCKET, SO_REUSEADDR, (int[]){ 1 }, sizeof(int))) {
    fprintf(stderr, "Could not SO_REUSEADDR socket.\n");
    goto fail;
  }
  if (setsockopt(
        masterSo, SOL_SOCKET, SO_REUSEPORT, (int[]){ 1 }, sizeof(int))) {
    fprintf(stderr, "Could not SO_REUSEPORT socket.\n");
    goto fail;
  }

  if (bind(masterSo, (const struct sockaddr*)&socka, sizeof socka)) {
    fprintf(stderr, "Bind has failed\n");
    goto fail;
  }

  if (listen(masterSo, MAX_SOCK)) {
    fprintf(stderr, "Listen has failed\n");
    goto fail;
  }
  for (nfds_t i = 0; i < maxSo; i++) {
    fds[i].fd = -1;
  }
  fds[MASTER_SOCKET].fd = masterSo;
  fds[MASTER_SOCKET].events = POLLIN;

  while (OK) {
    switch (poll(fds, maxSo, 1000)) {
      case 0: {
        printf("Timeout..\n");
        break;
      }

      case -1: {
        fprintf(stderr, "Error at poll.\n");
        pollfdCleanup(fds, maxSo);
        OK = 0;
        break;
      }

      default: {
        if (fds[MASTER_SOCKET].revents & POLLIN) {
          printf("Event on master..\n");
          if ((newSo = accept(masterSo, NULL, NULL)) >= 0) {
            printf("Accepted on master..\n");
            for (nfds_t i = 0; i < maxSo; i++) {
              if (fds[i].fd == -1) {
                fds[i].fd = newSo;
                fds[i].events = POLLIN;
                break;
              }
              if (i == MAX_SOCK) {
                close(newSo);
              }
            }
          } else {
            OK = 0;
            pollfdCleanup(fds, maxSo);
          }
        } else {
          for (nfds_t i = 0; i < maxSo; i++) {
            if (fds[i].fd != -1 && (fds[i].revents & POLLIN)) {
              printf("Data on fd %d, index %lu\n", fds[i].fd, i);
              citit = recv(fds[i].fd, buffer, MAX_BUFFER, 0);
              if (citit > 0) {
                buffer[citit] = '\0';
                printf("%s\n", buffer);
                send(fds[i].fd, buffer, citit, 0);
                if (!strncasecmp(buffer, "stop", strlen("stop"))) {
                  printf("Stopping..\n");
                  OK = 0;
                }
              } else if (citit == 0) {
                printf("EOF from %d\n", fds[i].fd);
                close(fds[i].fd);
                fds[i].fd = -1;
              } else {
                printf("Error from %d\n", fds[i].fd);
                close(fds[i].fd);
                fds[i].fd = -1;
              }
            }
          }
        }
      }
    }
  }

fail:
  if (masterSo >= 0) {
    close(masterSo);
  }
  return EXIT_FAILURE;
}
